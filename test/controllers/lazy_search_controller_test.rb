require 'test_helper'

class LazySearchControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  test "nearby users - cheating" do
    get :index
    assert_response :success
    assert_not_nil @controller.users
    puts ""
    puts "Our nearby users, sorted by `distance_km` (this was done by using the 'haversine' gem (CHEATING!)): #{@controller.nearby_sorted_users}"
    puts ""
  end

  test "nearby users - by hand" do
    get :by_hand
    assert_response :success
    finder = UsersFinder.new
    users = finder.users
    
    assert_not_nil @controller.nearby_sorted_users
    # This isn't a brilliant test, obviously, but I want to make sure I'm
    # not seeing *all* my users in here...
    assert_not_equal @controller.nearby_sorted_users, users
    puts ""
    puts "Our nearby users, sorted by `user_id`: #{@controller.nearby_sorted_users}"
    puts ""
  end

end
