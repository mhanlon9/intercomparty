class LazySearchController < ApplicationController
  attr_reader :users, :nearby_users, :nearby_sorted_users
  
def index
  @nearby_users = []
  finder = UsersFinder.new
  @users = finder.users
  @users.each do |next_user|
    next_lat = next_user["latitude"]
    next_long = next_user["longitude"]
    if !next_lat.nil? and !next_lat.nil?
      distance = Haversine.distance($intercom_office_latitude, $intercom_office_longitude, next_lat.to_d, next_long.to_d)
      next_user["distance_km"] = distance.to_kilometers
      if distance.to_kilometers <= 100
        @nearby_users << next_user
      end
    end
  end
  @nearby_sorted_users = @nearby_users.sort_by { |cust| cust["distance_km"] }
end

def by_hand
  finder = UsersFinder.new
  finder.find_nearby_users
  @nearby_sorted_users = finder.nearby_users.sort_by { |cust| cust["user_id"] }
end

def me
  lat_str = params[:latitude]
  long_str = params[:longitude]
  @am_nearby_enough = false
  @special_message = ""
  if !lat_str.nil? and !long_str.nil?
    finder = UsersFinder.new
    distance = finder.distance_in_km($intercom_office_latitude, $intercom_office_longitude, lat_str.to_d, long_str.to_d)
    if distance <= 100
      @am_nearby_enough = true
    end
  else
    @special_message = "And stop messing around with the parameters..."
  end
end
end