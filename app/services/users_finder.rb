class UsersFinder
  attr_reader :nearby_users
  
  $intercom_office_latitude = 53.3381985
  $intercom_office_longitude = ---6.2592576

  M_PI_180 = ( Math::PI / 180 )
  EARTH_RADIUS_KM = 6371
  
  
  def initialize()
    # We do nothing, because we really don't need to load our JSON
    # unless we're finding users from it.
  end

  def users
    if @users.nil?
      self.parseUsers
    end
    @users
  end

  def find_nearby_users(users=nil)
    if users.nil?
      self.parseUsers
    else
      @users = users
    end
    @nearby_users = []
    @users.each do |next_customer|
      next_lat = next_customer["latitude"]
      next_long = next_customer["longitude"]
      if !next_lat.nil? and !next_lat.nil?
        distance = self.distance_in_km($intercom_office_latitude, $intercom_office_longitude, next_lat.to_d, next_long.to_d)
        next_customer["distance_km"] = distance
        if distance <= 100
          @nearby_users << next_customer
        end
      end    
    end
  end
  
  def distance_in_km(lat1, long1, lat2, long2)
    longitude_diff = ( long2 - long1 )
    latitude_diff = ( lat2 - lat1 )
    # This marks the central angle, ala https://en.wikipedia.org/wiki/Great-circle_distance#Computational_formulas  
    a = ( Math.sin( ( latitude_diff * M_PI_180 ) / 2 )**2 + Math.cos( lat1 * M_PI_180 ) * Math.cos( lat2 * M_PI_180 ) * Math.sin( ( longitude_diff * M_PI_180 ) / 2 )**2 )
    c = 2 * Math.atan2( Math.sqrt( a ), Math.sqrt( 1 - a ) )
    # And then we figure the distance
    ( c * EARTH_RADIUS_KM )
  end
  
  def parseUsers
    @users = []
    File.open(Rails.root.join('db', 'customers.json').to_s, 'r') do |customers_file|
      customers_file.each do |line|
        next_customer = JSON.parse(line)
        @users << next_customer
      end
    end
  end
  
end